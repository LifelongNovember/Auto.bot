#!/usr/bin/env python
# -*- coding: utf-8 -*-

from core.db import db_fetch

async def checklist(ctx):
    returns = []
    responses = []

    returns.append(await is_logchannel_set(ctx, responses))
    returns.append(await is_cmdchannel_set(ctx, responses))

    value = returns[0]
    for i in range(1, len(returns)):
        value = value and returns[i]

    if value == False:
        for i in range(len(responses) - 1):
            await ctx.channel.send(responses[i])
        await ctx.respond(responses[-1])
    return value

async def is_logchannel_set(ctx, responses):
    if ctx.bot.cogs['Setup'].is_logchannel_set[ctx.guild_id]:
        return True
    else:
        responses.append("Aucun salon défini pour les messages de logs. Veuillez définir un salon avec la commande `/paramétrage logs`.")
        return False

async def is_cmdchannel_set(ctx, responses):
    if ctx.bot.cogs['Setup'].is_cmdchannel_set[ctx.guild_id]:
        return True
    else:
        responses.append("Aucun salon défini par défaut pour les commandes. Veuillez définir un salon avec la commande `/paramétrage cmd`.")
        return False
