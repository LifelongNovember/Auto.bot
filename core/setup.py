#!/usr/bin/env python
# -*- coding: utf-8 -*-

import discord
from discord.ext import commands
from discord.abc import GuildChannel
from discord import Role
from core.db import db_commit, db_fetch
from Auto import GUILDS, DEBUG

class Setup(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.is_logchannel_set = dict()
        self.is_cmdchannel_set = dict()
        self.is_dmchannel_set = dict()

    @commands.Cog.listener()
    async def on_ready(self):
        print(f"{self.bot.user} est en ligne ! ID : {self.bot.application_id}")
        
        await db_commit("CREATE TABLE IF NOT EXISTS special_channels(channel_id INTEGER PRIMARY KEY, designation TEXT, guild_id INTEGER, UNIQUE(channel_id, designation));")
        await db_commit("CREATE TABLE IF NOT EXISTS special_roles(role_id INTEGER PRIMARY KEY, designation TEXT, guild_id INTEGER)")

        for guild in self.bot.guilds:
            self.is_logchannel_set[guild.id] = await is_property_set(guild, "special_channels", "logs_channel", GuildChannel)
            self.is_cmdchannel_set[guild.id] = await is_property_set(guild, "special_channels", "cmd_channel", GuildChannel)
            self.is_dmchannel_set[guild.id] = await is_property_set(guild, "special_channels", "dm_channel", GuildChannel)

async def is_property_set(guild, table, designation, property_type):
    rows = await db_fetch(f"SELECT * FROM {table} WHERE designation = '{designation}' AND guild_id = {guild.id}")
    if len(rows) == 0:
        return False
    if property_type == GuildChannel:
        if guild.get_channel(rows[0]["channel_id"]) is None:
            return False
    elif property_type == Role:
        if guild.get_role(rows[0]["role_id"]) is None:
            return False
    return True

def setup(bot):
    bot.add_cog(Setup(bot))
