#!/usr/bin/env python
# -*- coding: utf-8 -*-

import discord
from discord import SlashCommandGroup, Option, TextChannel, ForumChannel
from discord.ext import commands
from Auto import DEBUG, GUILDS
from core.db import db_commit

class Settings(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    parametrage = SlashCommandGroup(
        name = "paramétrage",
        description = "Permet de modifier les paramètres du bot",
        default_member_permissions = discord.Permissions(administrator = True),
        guild_ids = DEBUG + GUILDS
    )

    @parametrage.command(
        name = "logs",
        description = "Définit le salon dédié aux messages de logs d'Auto"
    )
    async def logs(
        self,
        ctx: discord.ApplicationContext,
        channel: Option(TextChannel, name="salon", description="Le salon dans lequel Auto enverra ses logs")
    ):
        await db_commit(f"DELETE FROM special_channels WHERE designation = 'logs_channel' AND guild_id = '{ctx.guild_id}'")
        await db_commit(f"INSERT INTO special_channels (channel_id, designation, guild_id) VALUES ('{channel.id}', 'logs_channel', '{ctx.guild_id}')")
        self.bot.cogs['Setup'].is_logchannel_set[ctx.guild_id] = True
        await ctx.send_response(f"Canal de logs défini dans {channel.mention} !")

    @parametrage.command(
        name = "cmd",
        description = "Définit le salon dédié aux commandes, Auto établira certaines interactions dans ce salon"
    )
    async def cmd(
        self,
        ctx: discord.ApplicationContext,
        channel: Option(TextChannel, name="salon", description="Le salon dans lequel Auto établira ses interactions")
    ):
        await db_commit(f"DELETE FROM special_channels WHERE designation = 'cmd_channel' AND guild_id = '{ctx.guild_id}'")
        await db_commit(f"INSERT INTO special_channels (channel_id, designation, guild_id) VALUES ('{channel.id}', 'cmd_channel', '{ctx.guild_id}')")
        self.bot.cogs['Setup'].is_cmdchannel_set[ctx.guild_id] = True
        await ctx.send_response(f"Canal de commandes défini dans {channel.mention} !")

    @parametrage.command(
        name = "dm",
        description = "Définit le forum dédié à l'interface DM de modération"
    )
    async def dm(
        self,
        ctx: discord.ApplicationContext,
        channel: Option(ForumChannel, name="forum", description="Le channel forum dans lequel Auto inscrira les DM entrants")
    ):
        await db_commit(f"DELETE FROM special_channels WHERE designation = 'dm_channel' AND guild_id = '{ctx.guild_id}'")
        await db_commit(f"INSERT INTO special_channels (channel_id, designation, guild_id) VALUES ('{channel.id}', 'dm_channel', '{ctx.guild_id}')")
        self.bot.cogs['Setup'].is_dmchannel_set[ctx.guild_id] = True
        await ctx.send_response(f"Canal de DMs défini dans {channel.mention} !")

def setup(bot):
    bot.add_cog(Settings(bot))
