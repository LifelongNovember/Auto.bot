#!/usr/bin/env python
# -*- coding: utf-8 -*-

import aiosqlite

DB_PATH = "db.sql"

async def db_commit(query):
    async with aiosqlite.connect(DB_PATH) as db:
        await db.execute(query)
        await db.commit()

async def db_fetch(query):
    async with aiosqlite.connect(DB_PATH) as db:
        db.row_factory = aiosqlite.Row
        async with db.execute(query) as cursor:
            return await cursor.fetchall()
