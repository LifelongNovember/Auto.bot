#!/usr/bin/env python
# -*- coding: utf-8 -*-

import discord
from discord import option, Option, TextChannel, ChannelType
from discord.ext import commands
from typing import Union
from Auto import GUILDS, DEBUG
from core.checks import checklist
from core.db import db_fetch

class StartSendView(discord.ui.View):
    def __init__(self, bot, channel):
        super().__init__(timeout=None)
        self.bot = bot
        self.channel = channel

    @discord.ui.button(label="Rédiger", custom_id="write", style=discord.ButtonStyle.green)
    async def write_callback(self, button, interaction):
        await interaction.channel.send("Veuillez saisir votre message à la suite de ce fil.")
        await interaction.response.defer()
        await interaction.followup.edit_message(interaction.message.id, view=None)
        msg = await self.bot.wait_for("message")
        await interaction.channel.send("Votre message sera envoyé comme suit :")
        await interaction.channel.send(f"{msg.content}", view=SendView(bot = self.bot, channel = self.channel, content = msg.content))

    @discord.ui.button(label="Annuler", custom_id="cancel", style=discord.ButtonStyle.red)
    async def cancel_callback(self, button, interaction):
        await interaction.response.edit_message(view=None)
        await interaction.channel.delete()


class SendView(discord.ui.View):
    """Preview of the requested message with a 'Send' and a 'Cancel' button"""

    def __init__(self, bot, channel, content):
        super().__init__(timeout=None)
        self.bot = bot
        self.channel = channel
        self.content = content

    @discord.ui.button(label="Envoyer", custom_id="validate", style=discord.ButtonStyle.green)
    async def validate_callback(self, button, interaction):
        await self.channel.send(f"{self.content}")
        await interaction.channel.send(f"Le message a été envoyé dans {self.channel.mention} !")
        await interaction.response.edit_message(view=None)
        await interaction.channel.delete()

    @discord.ui.button(label="Modifier", custom_id="edit", style=discord.ButtonStyle.blurple)
    async def edit_callback(self, button, interaction):
        await interaction.channel.send("Veuillez saisir votre message à la suite de ce fil.")
        await interaction.response.defer()
        await interaction.followup.edit_message(interaction.message.id, view=None)
        msg = await self.bot.wait_for("message")
        await interaction.channel.send("Votre message sera envoyé comme suit :")
        await interaction.channel.send(f"{msg.content}", view=SendView(bot = self.bot, channel = self.channel, content = msg.content))

    @discord.ui.button(label="Annuler", custom_id="cancel", style=discord.ButtonStyle.red)
    async def cancel_callback(self, button, interaction):
        await interaction.channel.send("L'envoi a été annulé.")
        await interaction.response.edit_message(view=None)
        await interaction.channel.delete()


class StartEditView(discord.ui.View):
    def __init__(self, bot, content, message):
        super().__init__(timeout=None)
        self.bot = bot
        self.content = content
        self.orig_msg = message

    @discord.ui.button(label="Modifier", custom_id="edit", style=discord.ButtonStyle.green)
    async def edit_callback(self, button, interaction):
        await interaction.channel.send("Veuillez saisir votre message à la suite de ce fil.")
        await interaction.response.defer()
        await interaction.followup.edit_message(interaction.message.id, view=None)
        msg = await self.bot.wait_for("message")
        await interaction.channel.send("Votre message sera envoyé comme suit :")
        await interaction.channel.send(f"{msg.content}", view=EditView(bot = self.bot, content = msg.content, orig_msg = self.orig_msg))

    @discord.ui.button(label="Annuler", custom_id="cancel", style=discord.ButtonStyle.red)
    async def cancel_callback(self, button, interaction):
        await interaction.response.edit_message(view=None)
        await interaction.channel.delete()


class EditView(discord.ui.View):
    """Preview of the requested message with a 'Send' and a 'Cancel' button"""

    def __init__(self, bot, content, orig_msg):
        super().__init__(timeout=None)
        self.bot = bot
        self.content = content
        self.orig_msg = orig_msg

    @discord.ui.button(label="Envoyer", custom_id="validate", style=discord.ButtonStyle.green)
    async def validate_callback(self, button, interaction):
        await self.orig_msg.edit(f"{self.content}")
        await interaction.channel.send(f"Le message a été modifié !")
        await interaction.response.edit_message(view=None)
        await interaction.channel.delete()

    @discord.ui.button(label="Modifier", custom_id="edit_again", style=discord.ButtonStyle.blurple)
    async def edit_callback(self, button, interaction):
        await interaction.channel.send("Veuillez saisir votre message à la suite de ce fil.")
        await interaction.response.defer()
        await interaction.followup.edit_message(interaction.message.id, view=None)
        msg = await self.bot.wait_for("message")
        await interaction.channel.send("Votre message sera envoyé comme suit :")
        await interaction.channel.send(f"{msg.content}", view=EditView(bot = self.bot, content = msg.content, orig_msg = self.orig_msg))

    @discord.ui.button(label="Annuler", custom_id="cancel", style=discord.ButtonStyle.red)
    async def cancel_callback(self, button, interaction):
        await interaction.channel.send("La modification a été annulée.")
        await interaction.response.edit_message(view=None)
        await interaction.channel.delete()


class MSGBridge(commands.Cog):
    """Set of commands used to send messages on behalf of the bot"""

    def __init__(self, bot):
        self.bot = bot

    @commands.slash_command(
        name = "envoi_msg",
        description = "Envoie un message par l'intermédiaire d'Auto dans le salon renseigné en paramètre",
        guild_ids = GUILDS + DEBUG
    )
    @commands.check(checklist)
    @discord.default_permissions(administrator=True)
    async def send_msg(
        self,
        ctx: discord.ApplicationContext,
        channel: Option(TextChannel, name="salon", description="Le salon ou le fil dans lequel envoyer le message", channel_types=[ChannelType.public_thread, ChannelType.news, ChannelType.news_thread, ChannelType.forum]),
    ):
        rows = await db_fetch(f"SELECT * FROM special_channels WHERE designation = 'logs_channel' AND guild_id = {ctx.guild_id}")
        logs_channel = ctx.guild.get_channel(rows[0]["channel_id"])
        rows = await db_fetch(f"SELECT * FROM special_channels WHERE designation = 'cmd_channel' AND guild_id = {ctx.guild_id}")
        cmd_channel = ctx.guild.get_channel(rows[0]["channel_id"])
        
        thread = await cmd_channel.create_thread(name=f"[envoi_msg] #{channel.name}", type=ChannelType.public_thread)
        await thread.send(f"{ctx.user.mention} Envoi d'un message dans le salon {channel.mention}.", view=StartSendView(bot=self.bot, channel=channel))
        await ctx.send_response(f"Fil créé dans {cmd_channel.mention}.", ephemeral=True, delete_after=60.)


    @commands.message_command(
        name = "Modifier le message",
        description = "Modifie un message envoyé par l'intermédiaire d'Auto",
        guild_ids = GUILDS + DEBUG
    )
    @commands.check(checklist)
    @discord.default_permissions(administrator=True)
    async def edit_msg(
        self,
        ctx: discord.ApplicationContext,
        message: discord.Message
    ):
        rows = await db_fetch(f"SELECT * FROM special_channels WHERE designation = 'logs_channel' AND guild_id = {ctx.guild_id}")
        logs_channel = ctx.guild.get_channel(rows[0]["channel_id"])
        rows = await db_fetch(f"SELECT * FROM special_channels WHERE designation = 'cmd_channel' AND guild_id = {ctx.guild_id}")
        cmd_channel = ctx.guild.get_channel(rows[0]["channel_id"])

        if message.author != ctx.me:
            await ctx.send_response(f"Ce n'est pas moi qui ai envoyé ce message !", ephemeral=True, delete_after=60.)
            return

        thread = await cmd_channel.create_thread(name=f"[modif_msg] {message.id}", type=ChannelType.public_thread)
        await thread.send(f"{ctx.user.mention} voici le message original :")
        await thread.send(f"```\n{message.content}\n```")
        await thread.send(f"Commencer la modification :", view=StartEditView(bot=self.bot, message=message, content=message.content))
        await ctx.send_response(f"Fil créé dans {cmd_channel.mention}.", ephemeral=True, delete_after=60.)


    @commands.slash_command(
        name = "modif_msg",
        description = "Modifie le message dont l'ID est passé en paramètre",
        guild_ids = DEBUG + GUILDS
    )
    @commands.check(checklist)
    @discord.default_permissions(administrator=True)
    async def edit_msg_slash(
        self,
        ctx: discord.ApplicationContext,
        message_id: str
    ):
        rows = await db_fetch(f"SELECT * FROM special_channels WHERE designation = 'logs_channel' AND guild_id = {ctx.guild_id}")
        logs_channel = ctx.guild.get_channel(rows[0]["channel_id"])
        rows = await db_fetch(f"SELECT * FROM special_channels WHERE designation = 'cmd_channel' AND guild_id = {ctx.guild_id}")
        cmd_channel = ctx.guild.get_channel(rows[0]["channel_id"])
        message = await ctx.fetch_message(message_id)

        if message.author != ctx.me:
            await ctx.send_response(f"Ce n'est pas moi qui ai envoyé ce message !", ephemeral=True, delete_after=60.)
            return

        thread = await cmd_channel.create_thread(name=f"[modif_msg] {message.id}", type=ChannelType.public_thread)
        await thread.send(f"{ctx.user.mention} voici le message original :")
        await thread.send(f"```\n{message.content}\n```")
        await thread.send(f"Commencer la modification :", view=StartEditView(bot = self.bot, message=message, content=message.content))
        await ctx.send_response(f"Fil créé dans {cmd_channel.mention}.", ephemeral=True, delete_after=60.)


def setup(bot):
        bot.add_cog(MSGBridge(bot))
