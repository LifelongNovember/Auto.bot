#!/usr/bin/env python
# -*- coding: utf-8 -*-

import discord
from discord.ext import commands
from Auto import bot

class DMBridge(commands.Cog): 
    async def get_dm_channel(self):
        for guild in bot.guilds:
            if guild.id == 628373642014752779:
                G = guild
                break
        return G.get_channel_or_thread(1097534327664955583)

    @commands.Cog.listener()
    async def on_message(
        self,
        message: discord.Message
    ):
        if isinstance(message.channel, discord.channel.DMChannel):
            dm_channel = await self.get_dm_channel()
            await dm_channel.send(message.content)
    

def setup(bot):
    bot.add_cog(DMBridge(bot))
        
