#!/usr/bin/env python
# -*- coding: utf-8 -*-

import discord
import os
from dotenv import load_dotenv

intents = discord.Intents.all()
intents.members = True

load_dotenv()
TOKEN = os.getenv('TOKEN')
DEBUG = [int(i) for i in os.getenv('DEBUG').split(',')]
GUILDS = [int(i) for i in os.getenv('GUILDS').split(',')]

bot = discord.Bot(debug_guilds=DEBUG, intents=intents)

bot.load_extension("core.setup", store=False)
bot.load_extension("core.settings", store=False)
bot.load_extension("modules.msg_bridge.msg_bridge", store=False)
bot.load_extension("modules.DM_bridge.DM_bridge", store=False)

bot.run(TOKEN)
